﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfController : MonoBehaviour
{
    public GameObject bomb;
    public float minBombTime = 2;
    public float maxBombTime = 4;
    public float throughBombTime = 0.5f;
    private float bombTime = 0;
    private float lastBombTime = 0;
    private GameObject Sheep;
    private Animator anim;
    private GameObject gameController;

    // Start is called before the first frame update
    void Start()
    {
        UpdateBombTime();
        Sheep = GameObject.FindGameObjectWithTag("Player");
        anim = gameObject.GetComponent<Animator>();
        anim.SetBool("isBomb", false);
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= lastBombTime + bombTime - throughBombTime)
        {
            anim.SetBool("isBomb", true);
        }
        if (Time.time >= lastBombTime + bombTime)
        {
            ThroughBoom();
        }
    }

    void UpdateBombTime()
    {
        lastBombTime = Time.time;
        bombTime = Random.Range(minBombTime, maxBombTime);
    }
        
    void ThroughBoom()
    {
        GameObject bom = Instantiate(bomb,transform.position,Quaternion.identity);
        bom.GetComponent<BombController>().target = Sheep.transform.position; 
        UpdateBombTime();
        anim.SetBool("isBomb", false);
        gameController.GetComponent<GameController>().GetPoint();
    }
}
