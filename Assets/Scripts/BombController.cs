﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    public Vector3 target;
    public float moveSpeed = 5;
    public float destroyTime = 2;
    public GameObject explor;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destroyTime);
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate((transform.position - target) * moveSpeed * Time.deltaTime*-1);
        
    }
    void OnDestroy()
    {
        GameObject explors = Instantiate(explor, transform.position, Quaternion.identity);
        Destroy(explors,0.5f);
    }
}
 