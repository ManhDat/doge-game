﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject panelEndGame;
    public Button buttonRestart;
    public Sprite buttonIdle;
    public Sprite buttonHover;
    public Sprite buttonClick;
    public Text txtPoint;
    private int gamePoint;
    AudioSource audios;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        panelEndGame.SetActive(false);
        audios = gameObject.GetComponent<AudioSource>();
    }

    public void GetPoint()
    {
        gamePoint++;
        txtPoint.text = "Point: " + gamePoint.ToString();
    }
    public void ButtonHover()
    {
        buttonRestart.GetComponent<Image>().sprite = buttonHover;
    }
    public void ButtonIdle()
    {
        buttonRestart.GetComponent<Image>().sprite = buttonIdle;
    }
    public void ButtonClick()
    {
        buttonRestart.GetComponent<Image>().sprite = buttonClick;
    }

    public void StartGame()
    {
        SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EndGame()
    {
        audios.Play();
        Time.timeScale = 0;
        panelEndGame.SetActive(true);
    }
}
